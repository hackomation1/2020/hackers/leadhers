# PROFUSU

![tag](img/logo.png)

Electricity can be generated in different ways. One of the ways is through hydropower.
We are building a water turbine (hydropower plant) that can be use by everyone. It will be inexpensive so everybody can buy it.
This will be use to generate electricity for small devices when you are outdoor. This can be very usefull when you are at a place with no electricity. 

---

## Problem

**General Problem:** No Electricity outdoors.

**For tourists:** Most of the time when tourists go camping or there is no electricity. You start panicking about a low battery and want to charge your phone. And there is no electricity

## Solution

Building a water tribune that will generate electricity through the flow of water. It can charge small devices easily. And it is nature friendly.

## Features

- 24/7 Charging
- Power Without Maintenance
- A renewable energy source
- Profusu has a magnet coupling so it is easy to switch propellers depending on the flow of water (Based on a scalable parametric design)


## Deliverables

- **Project poster**

---
![tag](img/ed.png)

- **Proof of concept working**

---
Our dashboard that shows the percentage and voltage of the battery.

---

![tag](img/dash.png)

- **Product pitch deck**

---

[Pitch deck Profusu](https://docs.google.com/presentation/d/1KVwCAZpwWlOH4L1Q-ZjSHTBsmRVoA2AgrTwvLbSK_oU/edit#slide=id.gada2e0dc54_2_3)


## Product

- Fully Build  Mid Size waterturbine with floats (main customer: Resort Owners)

---

![tag](img/70.jpg)

--- 
## The team & contact

---

![tag](img/crew.png)

---

- Shewishka Ganpat [suraisha1996@hotmail.com]
- Devika Mahabir [sarishma-mahabir@gmail.com]
- Anjessa Linger [Anjessalinger1@gmail.com]
