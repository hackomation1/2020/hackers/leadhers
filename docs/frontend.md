# 3. Front-End & Platform

**Create a webserver with websockets using an ESP32  in Arduino for displaying ou Voltage and Battery percentage on a Dashboard**

Rather than just host a simple web page, we’re going to build on the WebSocket idea. Let’s have our ESP32 be an access point (AP) and host a web page. 
When a browser requests that page, the ESP32 will serve it. As soon as the page loads, the client will immediately make a WebSocket connection back to the ESP32. 
That allows us to have fast control of our hardware connected to the ESP32.


## Steps taken, Testing & Problems, Proof of work

**Install SPIFFS Plugin**

The Serial Peripheral Interface Flash File System, or SPIFFS for short. It's a light-weight file system for microcontrollers with an SPI flash chip. 
SPIFFS let's you access the flash memory as if it was a normal file system like the one on your computer (but much simpler of course): you can read and write files, create folders ...

- We need to use a special program to upload files over SPI.
- Go to [Arduino ESP32fs plugin](https://github.com/me-no-dev/arduino-esp32fs-plugin) and follow the instructions to install the Arduino plugin.

The instructions: **Arduino ESP32 filesystem uploader**

- Arduino plugin which packs sketch data folder into SPIFFS filesystem image, and uploads the image to ESP32 flash memory.

**Installation**

- Make sure you use one of the supported versions of Arduino IDE and have ESP32 core installed.
- Download the tool archive from [Release page](https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/tag/1.0)

---

![19](img/19.jpg)

---

- In your Arduino sketchbook directory, create tools directory if it doesn't exist yet.
  In my Arduino directory, the tool directory was already created
- Unpack the tool into tools directory (by me the path look like <This Pc>/Documents/Arduino/tools/ESP32FS/tool/esp32fs.jar).

---

![20](img/20.jpg)

---

- Restart Arduino IDE

- To check if the plugin was successfully installed, open your Arduino IDE. Select your **ESP32 board**, go to **Tools** and check that you have the option **“ESP32 Sketch Data Upload“.**

---

![21](img/21.jpg)

---

**Usage**

- Open a sketch (or create a new one and save it). For this I opened a new sketch in Arduino IDE.
- For this click on **File**-> Then **New**

---

![22](img/22.jpg)

---

- Delete the code for the sketch and upload the code for the webserver

*The code*

```ruby

#include <WiFi.h>
#include <SPIFFS.h>
#include <ESPAsyncWebServer.h>
#include <WebSocketsServer.h>


// Constants
const char *ssid = "PROFUSU";
const char *password =  "hacker";
const char *msg_toggle_led = "toggleLED";
const char *msg_get_led = "getLEDState";
const char *msg_get_BATV = "getBattVolt";
const char *msg_get_perc = "getBattPerc";
const int dns_port = 53;
const int http_port = 80;
const int ws_port = 1337;
const int led_pin = 15;
const int vbatPin = 33; 

// Globals
AsyncWebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(1337);
char msg_buf[10];
int led_state = 0;

// Battery globals
float VBAT =0;
float PBAT =0;
int battPerc=0;

/***********************************************************
 * Functions
 */

// Callback: receiving any WebSocket message
void onWebSocketEvent(uint8_t client_num,
                      WStype_t type,
                      uint8_t * payload,
                      size_t length) {

  // Figure out the type of WebSocket event
  switch(type) {

    // Client has disconnected
    case WStype_DISCONNECTED:
      Serial.printf("[%u] Disconnected!\n", client_num);
      break;

    // New client has connected
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(client_num);
        Serial.printf("[%u] Connection from ", client_num);
        Serial.println(ip.toString());
      }
      break;

    // Handle text messages from client
    case WStype_TEXT:

      // Print out raw message
      Serial.printf("[%u] Received text: %s\n", client_num, payload);

      // Toggle LED
      if ( strcmp((char *)payload, "toggleLED") == 0 ) {
        led_state = led_state ? 0 : 1;
        Serial.printf("Toggling LED to %u\n", led_state);
        digitalWrite(led_pin, led_state);

      // Report the state of the LED
      } else if ( strcmp((char *)payload, "getLEDState") == 0 ) {
        sprintf(msg_buf, "%d", led_state);
        Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);

      // Report the battery Voltage
      } else if ( strcmp((char *)payload, "getBattVolt") == 0 ) {
        sprintf(msg_buf, "%d", led_state);
        Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);

      // Report battery percantage
      } else if ( strcmp((char *)payload, "getBattPerc") == 0 ) {
        sprintf(msg_buf, "%d", led_state);
        Serial.printf("Sending to [%u]: %s\n", client_num, msg_buf);
        webSocket.sendTXT(client_num, msg_buf);

      // Message not recognized
      } else {
        Serial.println("[%u] Message not recognized");
      }
      break;

    // For everything else: do nothing
    case WStype_BIN:
    case WStype_ERROR:
    case WStype_FRAGMENT_TEXT_START:
    case WStype_FRAGMENT_BIN_START:
    case WStype_FRAGMENT:
    case WStype_FRAGMENT_FIN:
    default:
      break;
  }
}

// Callback: send homepage
void onIndexRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/index.html", "text/html");
}
// Callback: send jquery
void onJQRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/jquery.js", "text/js");
}

// Callback: send stylonCSSRequeste sheet
void onCSSRequest(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(SPIFFS, "/style.css", "text/css");
}

// Callback: send 404 if requested file does not exist
void onPageNotFound(AsyncWebServerRequest *request) {
  IPAddress remote_ip = request->client()->remoteIP();
  Serial.println("[" + remote_ip.toString() +
                  "] HTTP GET request of " + request->url());
  request->send(404, "text/plain", "Not found");
}



/***********************************************************
 * Main
 */

void setup() {
  // Init LED and turn off
  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, LOW);

  // Start Serial port
  Serial.begin(115200);

  // Make sure we can read the file system
  if( !SPIFFS.begin()){
    Serial.println("Error mounting SPIFFS");
    while(1);
  }

  // Start access point
  WiFi.softAP(ssid, password);

  // Print our IP address
  Serial.println();
  Serial.println("AP running");
  Serial.print("My IP address: ");
  Serial.println(WiFi.softAPIP());

  // On HTTP request for root, provide index.html file
  server.on("/", HTTP_GET, onIndexRequest);

  // On HTTP request for style sheet, provide style.css
  server.on("/style.css", HTTP_GET, onCSSRequest);

    // On HTTP request for style sheet, provide style.css
  server.on("/jquery.js", HTTP_GET, onJQRequest);

  // Handle requests for pages that do not exist
  server.onNotFound(onPageNotFound);

server.on("/voltage", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", String(VBAT,2));
});
server.on("/percentage", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", String(PBAT,2));
});
server.on("/img", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(SPIFFS, "/logo.jpg", "image/jpg");
});

  // Start web server
  server.begin();

  // Start WebSocket server and assign callback
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);
  
}

void loop() {
  
  // Look for and handle WebSocket data
  webSocket.loop();
  VBAT = (float)(analogRead(vbatPin)) / 4095*2*3.3*1.1;
  PBAT = ((VBAT-3)/0.9)*100;
  if(PBAT >100){
    PBAT =100;
  }
}

```
- After putting the webserver code. Save it and give it a name **"webserver"**

---

![23](img/23.jpg)

---

- After saving it go back to Arduino IDE and then go to **sketch directory** (choose **Sketch** > Show **Sketch Folder**). Make sure you see where the sketch folder is.
- Mine is in This **Pc** -> **Documents** -> **Arduino** -> **webserver folder**.

---

![25](img/25.jpg)

---

- In the webserver folder create another folder and name it **"data"**. This folder is needed to put the files in it for the filesystem upload.
  
---

![26](img/26.jpg)

---

**Install Arduino Libraries**

- Now I need libraries to let the webserver and websockets work.

**I just downloaded these three libraries:**

- [AsyncTCP](https://github.com/me-no-dev/AsyncTCP)

---

![27](img/27.jpg)

---

- [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer)

- [arduinoWebSockets](https://github.com/Links2004/arduinoWebSockets)

---

Now use the same Webserver code and take note of the SSID and password. We will need these to connect our phone/computer to the ESP32’s AP.

```ruby
const char *ssid = "ESP32-AP";
const char *password = "letmeinplz"; 

```

You can enter your own password. **"Just remember it"**

- After editing the code, I included the libraries that I downloaded.

- For this go to **Sketch** -> **Include Library** -> **Add Zip library**

---

![28](img/28.jpg)

---

Do this for all the three libraries

- I then uploaded my code to my ESP32 by pressing on the boot button that is on my ESP32.

I succesfully uploaded my code.

---

![29](img/29.jpg)

---

- Open a serial monitor with a baud rate of 115200. 

---

![30](img/30.jpg)

---

You should see the IP address of the ESP32 printed to the screen. 
When you run the Arduino soft access point software on the ESP32, the default IP address is **192.168.4.1.**

---

![30](img/31.jpg)

---

**Webpage Code**

For the webpage code I created a file in the data folder that I created before in the webserver folder.
- Go in the data folder and create a **file**. Name it **index.html**. In this data folder you can also add the **CSS** file. I didn't do that just to make it simple.

---

![32](img/32.jpg)

---

- After I created the index.html. Open the index.html with notepad or any editor. 
- Edit this code in index.html

```html

<html>
  <head>
  <title>Display Battery Status using HTML and CSS</title>
      <link href="style.css" rel="stylesheet" type="text/css">  
	  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

<body>
	</div>
	<p>
      <h2 class="text-align:left "> Battery Status  </h2>
	</p>

	<link href='https://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>
	<div class="volt">
		<ul class="meter">
			<li class="low"></li>
			<li class="normal"></li>
			<li class="high"></li>
		</ul>

		<div class="dial">
				<div class="inner">
					<div class="arrow">
					</div>
				</div>			
		</div>

		<div class="value">
			0 V
		</div>

	</div>
	<div class="perc">
		<ul class="meter">
			<li class="low"></li>
			<li class="normal"></li>
			<li class="high"></li>
		</ul>

		<div class="dial">
				<div class="inner">
					<div class="arrow">
					</div>
				</div>			
		</div>

		<div class="value">
			0%
		</div>
	</div>

		<script type="text/javascript" src="jquery.js"></script>

	<script>
	$( document ).ready(function() {
		var dialV = $(".volt .dial .inner");
		var dialP = $(".perc .dial .inner");
		var gauge_volt= $(".volt .value");
		var gauge_perc= $(".perc .value");
		let endpointV = '/voltage';
		let endpointP = '/percentage'

		function rotateDial()
		{
			var deg = 0;
			var volt = 0;
			var perc = 0;
			// show voltage
			$.ajax({
			  url: endpointV,
			  contentType: "application/json",
			  dataType: 'json',
			  success: function(result){
				console.log(result);
				//var valueV = result;
				degV = 180*(result/12);
				gauge_volt.html(result + " Volt");
				dialV.css({'transform': 'rotate('+degV+'deg)'});
				dialV.css({'-ms-transform': 'rotate('+degV+'deg)'});
				dialV.css({'-moz-transform': 'rotate('+degV+'deg)'});
				dialV.css({'-o-transform': 'rotate('+degV+'deg)'}); 
				dialV.css({'-webkit-transform': 'rotate('+degV+'deg)'});
			  }
			})
			$.ajax({
			  url: endpointP,
			  contentType: "application/json",
			  dataType: 'json',
			  success: function(result){
				console.log(result);
				value = result;
				deg = (value * 177.5) / 100;
				gauge_perc.html(value + "%");
				dialP.css({'transform': 'rotate('+deg+'deg)'});
				dialP.css({'-ms-transform': 'rotate('+deg+'deg)'});
				dialP.css({'-moz-transform': 'rotate('+deg+'deg)'});
				dialP.css({'-o-transform': 'rotate('+deg+'deg)'}); 
				dialP.css({'-webkit-transform': 'rotate('+deg+'deg)'});
			  }
			})
		}
		setInterval(rotateDial, 2000);
	});
	</script>
				
</body>
</html>

```

---

**The style.css code**


```css

	html, body
	
text {
				 
   text-align:center;
   color: Green;
}
  

#centeredpage {
   width:100%;
   margin:0 auto;
   text-align:left;
   position:relative;
  
}
			{
				padding: 0;
				margin: 0;
			}

			body,
		
		    .dial
			{
				background-color: black;

			}

			.gauge
			{
				position: fixed;
				width: 500px;
				height: 500px;
				top: 100px;
				left: 50%;
				margin-left: -250px;
				border-radius: 100%;
				transform-origin: 50% 50%;
				-webkit-transform-origin: 50% 50%;
				-ms-transform-origin: 50% 50%;
				-webkit-transform: rotate(0deg);

			}

			.meter
			{
				margin: 0;
				padding: 0;
			}

			.meter > li
			{
				width: 250px;
				height: 250px;
				list-style-type: none;
				position: absolute;
				border-top-left-radius: 250px;
				border-top-right-radius: 0px;
				transform-origin:  100% 100%;;
				-webkit-transform-origin:  100% 100%;;
				-ms-transform-origin:  100% 100%;;
				transition-property: -webkit-transform;
				pointer-events: none;
			}

			.meter .low
			{
				transform: rotate(0deg); /* W3C */
				-webkit-transform: rotate(0deg); /* Safari & Chrome */
				-moz-transform: rotate(0deg); /* Firefox */
				-ms-transform: rotate(0deg); /* Internet Explorer */
				-o-transform: rotate(0deg); /* Opera */
				z-index: 8;
				background-color: #09B84F;
			}

			.meter .normal
			{
				transform: rotate(47deg); /* W3C */
				-webkit-transform: rotate(47deg); /* Safari & Chrome */
				-moz-transform: rotate(47deg); /* Firefox */
				-ms-transform: rotate(47deg); /* Internet Explorer */
				-o-transform: rotate(47deg); /* Opera */
				z-index: 7;
				background-color: #FEE62A;
			}

			.meter .high
			{
				transform: rotate(90deg); /* W3C */
				-webkit-transform: rotate(90deg); /* Safari & Chrome */
				-moz-transform: rotate(90deg); /* Firefox */
				-ms-transform: rotate(90deg); /* Internet Explorer */
				-o-transform: rotate(90deg); /* Opera */
				z-index: 6;
				background-color: #FA0E1C;
			}

			
			.dial,
			.dial .inner
			{
				width: 470px;
				height: 470px;
				position: relative;
				top: 10px;
				left: 5px;
				border-radius: 100%;
				border-color: purple;
				z-index: 10;
				transition-property: -webkit-transform;
				transition-duration: 1s;
				transition-timing-function: ease-in-out;
				transform: rotate(0deg); /* W3C */
				-webkit-transform: rotate(0deg); /* Safari & Chrome */
				-moz-transform: rotate(0deg); /* Firefox */
				-ms-transform: rotate(0deg); /* Internet Explorer */
				-o-transform: rotate(0deg); /* Opera */
			}

			.dial .arrow
			{
				width: 0; 
				height: 0; 
				position: absolute;
				top: 214px;
				left: 24px;
				border-left: 5px solid transparent;
				border-right: 5px solid transparent;
				border-bottom: 32px solid white;
				-webkit-transform: rotate(-88deg); /* Safari & Chrome */
				-moz-transform: rotate(88deg); /* Firefox */
				-ms-transform: rotate(88deg); /* Internet Explorer */
				-o-transform: rotate(88deg); /* Opera */

			}

			.gauge .value
			{
				font-family: 'Josefin Slab', serif;
				font-size: 50px;
				color: white;
				position: absolute;
				top: 142px;
				left: 45%;
				z-index: 11;
			}
			
			.volt .value
			{
				font-family: 'Josefin Slab', serif;
				font-size: 50px;
				color: white;
				position: absolute;
				top: 185px;
				left: 13%;
				z-index: 11;
			}
			.perc .value
			{
				font-family: 'Josefin Slab', serif;
				font-size: 50px;
				color: white;
				position: absolute;
				top: 700px;
				left: 15%;
				z-index: 11;
			}
			
			}

```

- Save this and go the Arduino IDE. The ESP32 shoulde be plugged in.
- Go to **Tools** -> Click **ESP32 Sketch Data Upload**

---

![33](img/33.jpg)

---

- By clicking that the image will upload. The SPIFFS succesfully uploaded

---

![34](img/34.jpg)

---

**Run it!**

- With the index.html file uploaded and the Arduino code running, you should be able to connect to the ESP32’s access point. 
- Using the computer, search for open WiFi access points and connect to the one named ESP32-AP. When asked for a password, enter **"hacker"**. We need to be connected to the ESP32 to open our dashboard.

---

![35](img/35.jpg)

---

- After connected to the ESp32 I opened my dashboard and it looks like this.

---

![36](img/36.jpg)


## Files & Code

- Here in this file are all the codes.

<a href="myFile.js" download>Codes</a>


## References & Credits

Above we have all the refrences
