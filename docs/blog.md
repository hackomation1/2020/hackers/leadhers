# 5. Blog 

**Digital Manufacturing**

We started designing our propeller and water turbine in OpenScad. It is a paramateric design and it was easy to scale it.

- First we design how our waterturbine will look.

---

![39](img/39.jpg)

---

![tag](img/prop.jpg)

---

## We design different propellers for different uses

---

![40](img/40.jpg)

---

![41](img/41.jpg)

---

![42](img/42.jpg)

---

![43](img/43.jpg)

---

## We printed two propellers to see if both work

---

![44](img/44.jpg)

---

![45](img/45.jpg)

---

![46](img/46.jpg)

---

- **Testing propeller**

[https://www.youtube.com/watch?v=rdhVggybD6w&feature=youtu.be](https://www.youtube.com/watch?v=rdhVggybD6w&feature=youtu.be)

![tag](img/proptest.PNG)

---

- **Testing propeller With motor what it would be like in the water**

- **You can see the motor rotating also**

[https://www.youtube.com/watch?v=d14za6EoeMs&feature=youtu.be](https://www.youtube.com/watch?v=d14za6EoeMs&feature=youtu.be)

![tag](img/proptest1.PNG)


## We printed our other materials that we needed for our waterturbine

- For the motor we used 2 pvc pipe coupling

---

![47](img/47.jpg)

---

## Designing in Thinkercad*

- We designed the ring, the lid, the magnet motor holder, the stand for holding the waterturbine.

---

![48](img/48.jpg)

---

![53](img/53.jpg)

---

## After designing. We export the stl file and load it to cura to print it on a 3d printer.

- Cura is an open source slicing application for 3D printers.

---

![49](img/49.jpg)

---

![50](img/50.jpg)

---

![54](img/54.jpg)

---


## The other part of the magnet holder that is connected to the propeller

![55](img/55.jpg)

---

![56](img/56.jpg)

---


## The ring and the magnet motor holder

![51](img/51.jpg)

---

![52](img/52.jpg)

---


## After designing and printing everything, we cut a piece of pipe to put the turbine in it and 2 pipe for the floats

![57](img/57.jpg)

---

![58](img/58.jpg)

---


## After cuttting the pipes we cleaned it up and paint it

![59](img/59.jpg)

---

![60](img/60.jpg)

---

![61](img/61.jpg)

---

## **The enclosure**

- We designed our enclosure in thinkercad

---

![62](img/62.jpg)

---

- Export stl fil to cura

---

![63](img/63.jpg)

---

- On the 3d printer

---

![64](img/64.jpg)

---

- The eclosure came out like this

---

![65](img/65.jpg)

---

![105](img/105.jpg)

---

## We put them together

![100](img/100.jpg)

---

![101](img/101.jpg)

---

![102](img/102.jpg)

---

## With the magnet

![103](img/103.jpg)

---

![104](img/104.jpg)

---


### The product

- After designing, printing, building our stack and made our electronic design we set everuthing together. We made sure that the DC motor is waterproof. So there won't go any water in it. We also made the floats for the waterturbine.

---

![70](img/70.jpg)

---

![71](img/71.jpg)

---

- Hackomation Final
- Product pitch 

---

![tag](img/106.jpg)

---

![tag](img/107.jpg)

---

![tag](img/108.jpg)
