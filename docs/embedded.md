# 2. Embedded solution

First we started to research about our product and how we can make it. We searched how it will look and what we will need to make it.
We put our mind to it and planned to use a DC motor to make our propeller rotate.

## 2.1 Objectives

For making our waterturbine we used the following components:

- diodes
- 5v regulator
- dc motor
- charge circuit
- esp32(we are using the ttgo) for the dashboard

We try to build the diode bridge and try to let it work with the dc motor:

![tag](img/2.jpg)


## 2.3 Steps taken

We used the **TTGO Esp32**

**ESP32** is a series of low-cost, low-power system on a chip microcontrollers with integrated Wi-Fi and dual-mode Bluetooth. 
The ESP32 series employs a Tensilica Xtensa LX6 microprocessor in both dual-core and single-core variations and includes built-in antenna switches, 
RF balun, power amplifier, low-noise receive amplifier, filters, and power-management modules

**The features of the ESP32:**

**Processors:**

- CPU: Xtensa dual-core (or single-core) 32-bit LX6 microprocessor, operating at 160 or 240 MHz and performing at up to 600 DMIPS
- Ultra low power (ULP) co-processor

**Memory:** 520 KiB SRAM

**Wireless connectivity:**

- Wi-Fi: 802.11 b/g/n
- Bluetooth: v4.2 BR/EDR and BLE (shares the radio with Wi-Fi)

**Peripheral interfaces:**

- 12-bit SAR ADC up to 18 channels
- 2 × 8-bit DACs
- 10 × touch sensors (capacitive sensing GPIOs)
- 4 × SPI
- 2 × I²S interfaces
- 2 × I²C interfaces
- 3 × UART
- SD/SDIO/CE-ATA/MMC/eMMC host controller
- SDIO/SPI slave controller
- Ethernet MAC interface with dedicated DMA and IEEE 1588 Precision Time Protocol support
- CAN bus 2.0
- Infrared remote controller (TX/RX, up to 8 channels)
- Motor PWM
- LED PWM (up to 16 channels)
- Hall effect sensor
- Ultra low power analog pre-amplifier

**Security:**

- IEEE 802.11 standard security features all supported, including WFA, WPA/WPA2 and WAPI
- Secure boot
- Flash encryption
- 1024-bit OTP, up to 768-bit for customers
- Cryptographic hardware acceleration: AES, SHA-2, RSA, elliptic curve cryptography (ECC), random number generator (RNG)

**Power management:**

- Internal low-dropout regulator
- Individual power domain for RTC
- 5µA deep sleep current
- Wake up from GPIO interrupt, timer, ADC measurements, capacitive touch sensor interrupt

---
![tag](img/3.jpg)


## 2.4  Testing & Problems, Proof of Work

The headers were not connected to the Esp32. So we soldered the headers first.

---

![4](img/4.jpg)

![5](img/5.jpg)

---

**Setup Esp32 to Arduino IDE**

For the set up we need to install the **ESP32 in my Arduino IDE**

- Open **Arduino IDE** -> Then go to **file** -> And then **preferences**

---
![tag](img/6.jpg)

---

- Enter [](https://dl.espressif.com/dl/package_esp32_index.json) into the **“Additional Board Manager URLs”** field as shown in the figure below. 
- Then, click the **“OK”** button

![tag](img/7.jpg)

---

- After that open the **Board Manager**. For that go to **tools** -> **Board** -> Click on **Boards Managers**

![tag](img/8.jpg)

---

- The Board Managers screen will open and then search for the **ESP32 library** to install it

![tag](img/9.jpg)

---
After a few secondes the Library will be installed.
Now we can use the ESP32.

---

### ESP32 Basic Over The Air (OTA) Programming In Arduino IDE

A fantastic feature of any WiFi-enabled microcontroller like ESP32 is the ability to update its firmware wirelessly. This is known as Over-The-Air (OTA) programming.

---

**What is OTA programming in ESP32?**

The OTA programming allows updating/uploading a new program to ESP32 using Wi-Fi instead of requiring the user to connect the ESP32 to a computer via USB to perform the update.
OTA functionality is extremely useful in case of no physical access to the ESP module. It helps reduce the amount of time spent for updating each ESP module at the time of maintenance.

One important feature of OTA is that one central location can send an update to multiple ESPs sharing same network.
The only disadvantage is that you have to add an extra code for OTA with every sketch you upload, so that you’re able to use OTA in the next update.

---

*Ways To Implement OTA In ESP32*

There are two ways to implement OTA functionality in ESP32.

- Basic OTA – Over-the-air updates are sent through Arduino IDE.
- Web Updater OTA – Over-the-air updates are sent through a web browser.

Each one has its own advantages. You can implement any one according to your project’s requirement.

---

**3 Simple Steps To Use Basic OTA with ESP32**

- Install Python 2.7.x seriesThe first step is to install Python 2.7.x series in your computer.
- Upload Basic OTA Firmware SeriallyUpload the sketch containing OTA firmware serially. It’s a mandatory step, so that you’re able to do the next updates/uploads over-the-air.
- Upload New Sketch Over-The-AirNow, you can upload new sketches to the ESP32 from Arduino IDE over-the-air.

---

**Step 1: Install Python 2.7.x series**

In order to use OTA functionality, you need to install the Python 2.7.x version, if not already installed on your machine.

Go to [Python's official website](https://www.python.org/downloads/) and download 2.7.x (specific release) for Windows (MSI installer)
Open the installer and follow the installation wizard.
In Customize Python 2.7.X section, make sure the last option Add python.exe to Path is enabled.

Make sure your ESP32 is connected with your computer.

---

**Step 2: Upload OTA Routine Serially**

The factory image in ESP32 doesn’t have an OTA Upgrade capability. So, you need to load the OTA firmware on the ESP32 through serial interface first.
It’s a mandatory step to initially update the firmware, so that you’re able to do the next updates/uploads over-the-air.

The ESP32 add-on for the Arduino IDE comes with a OTA library & BasicOTA example. For the basicOTA example gor File.

- You can access it through **File** -> **Examples** -> **ArduinoOTA** -> **BasicOTA.**

---

![tag](img/10.jpg)

---

The following code should load. But, before you head for uploading the sketch, you need to make some changes to make it work for you. 
You need to modify the following two variables with your network credentials, so that ESP32 can establish a connection with existing network.

---

```ruby
const char* ssid = "telew_f28";
const char* password = "(your code)";

```
---

**The BasicOTA code:**

```ruby
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "..........";
const char* password = "..........";

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();
}

```
---

- After that I uploaded my sketch to the ESP32 > **Before uploading make sure you select you port.** For that go to **tools** and **select your port.**

While uploading I got an error:

![tag](img/11.jpg)

---

I research my error and it means that it failed to connect to the board so I had to press my boot/flash bottton on my ESP32 while uploading.

- If everything is done properly, open the Serial Monitor at a baud rate of 115200. And press the EN button on ESP32. 
- If everything is OK, it will output the dynamic IP address obtained from your router. Note it down-

---
![tag](img/12.jpg)

---

Now we can use the ESP32 On The Air.

**Display a Text On The Oled with OTA**

We wanted to try display a text on the Oled and we did some research. We first downloaded a file that we used for the Oled. We search for 
**ESP32-OLED0.96-ssd1306** and downloaded it.

---
![tag](img/13.jpg)

---

I added this file in my Arduino Folder that is in my Documents.

- After adding the File I open it: 

---
![14](img/14.jpg)

---

- Before Uploading the sketch I need to install the library for the Oled. I had already installed the library, but I will put the names for the library that I used.

- To install the library for the OLED display I clicked on **Sketch**-> **Include Library**-> **Manage Library**

---

![15](img/15.jpg)

---

- Then I searched for **SSD1306** Library and installed it:

---

![16](img/16.jpg)

---

I wanted the Oled to work over OTA. So I had to Combine the SSD1306SimpleDemo Sketch into my BasicOtA Sketch. This is the same sketch I used to blink the LED.
I combined to sketches into one.

---

**The complete sketch for the Oled with OTA**

```ruby
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "images.h"

const char* ssid = "telew_f28";
const char* password = "your password";

//variabls for blinking an LED with Millis
const int led = 16; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

SSD1306  display(0x3c, 5, 4);

#define DEMO_DURATION 3000
typedef void (*Demo)(void);

int demoMode = 0;
int counter = 1;

void setup() {
  
  // Initialising the UI will init the display too.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  pinMode(led, OUTPUT);
  
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void drawFontFaceDemo() {
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Hello world");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Hello world");
    display.setFont(ArialMT_Plain_24);
    display.drawString(0, 26, "Shewishka");
}

void drawTextFlowDemo() {
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawStringMaxWidth(0, 0, 128,
      "Lorem ipsum\n dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore." );
}

void drawTextAlignmentDemo() {
    // Text alignment demo
  display.setFont(ArialMT_Plain_10);

  // The coordinates define the left starting point of the text
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 10, "Left aligned (0,10)");

  // The coordinates define the center of the text
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 22, "Center aligned (64,22)");

  // The coordinates define the right end of the text
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(128, 33, "Right aligned (128,33)");
}

void drawRectDemo() {
      // Draw a pixel at given position
    for (int i = 0; i < 10; i++) {
      display.setPixel(i, i);
      display.setPixel(10 - i, i);
    }
    display.drawRect(12, 12, 20, 20);

    // Fill the rectangle
    display.fillRect(14, 14, 17, 17);

    // Draw a line horizontally
    display.drawHorizontalLine(0, 40, 20);

    // Draw a line horizontally
    display.drawVerticalLine(40, 0, 20);
}

void drawCircleDemo() {
  for (int i=1; i < 8; i++) {
    display.setColor(WHITE);
    display.drawCircle(32, 32, i*3);
    if (i % 2 == 0) {
      display.setColor(BLACK);
    }
    display.fillCircle(96, 32, 32 - i* 3);
  }
}

void drawProgressBarDemo() {
  int progress = (counter / 5) % 100;
  // draw the progress bar
  display.drawProgressBar(0, 32, 120, 10, progress);

  // draw the percentage as String
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 15, String(progress) + "%");
}

void drawImageDemo() {
    // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
    // on how to create xbm files
    display.drawXbm(34, 14, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

Demo demos[] = {drawFontFaceDemo, drawTextFlowDemo, drawTextAlignmentDemo, drawRectDemo, drawCircleDemo, drawProgressBarDemo, drawImageDemo};
int demoLength = (sizeof(demos) / sizeof(Demo));
long timeSinceLastModeSwitch = 0;


void loop() {
  ArduinoOTA.handle();
    
//loop to blink without delay
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
  // save the last time you blinked the LED
  previousMillis = currentMillis;
  // if the LED is off turn it on and vice-versa:
  ledState = not(ledState);
  // set the LED with the ledState of the variable:
  digitalWrite(led,  ledState);
  }
  
    // clear the display
  display.clear();
  // draw the current demo method
  demos[demoMode]();

  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(10, 128, String(millis()));
  // write the buffer to the display
  display.display();

  if (millis() - timeSinceLastModeSwitch > DEMO_DURATION) {
    demoMode = (demoMode + 1)  % demoLength;
    timeSinceLastModeSwitch = millis();
  }
  counter++;
  delay(10);
}

```
After doing this I selected my board:*ESP32 Dev Module* and I selected my Port. Then I compiled and uploaded my Sketch. 
While uploading I got alot of errors in my code so I had to reorder my code till it was fine to upload. 

Then finally I uploaded my code. And I was glad it worked.

---

![17](img/17.jpg)

---

- I used this example to see if the Oled works. After this I used the same code and recode it for the Voltage of the battery.

**Battery Voltage and Status on OLED**

After testing everything I can use the same steps for displaying the battery voltage and the status. I used the code for the Oled that I tried before. And recode it.

- I managed to display the voltage on the Oled display

*The code We used*

```ruby

#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "images.h"
#include "U8glib.h"

U8GLIB_SSD1306_ADAFRUIT_128X64 u8g(9, 10, 15, 14);  

const uint8_t blue = 2;
const uint8_t vbatPin = 35;
float VBAT;  // battery voltage from ESP32 ADC read

const char* ssid = "IOTLAB_GUEST";
const char* password = "IoT@Guest";

//variabls for blinking an LED with Millis
const int led = 16; // ESP32 Pin to which onboard LED is connected
unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;  // interval at which to blink (milliseconds)
int ledState = LOW;  // ledState used to set the LED

SSD1306  display(0x3c, 5, 4);

#define DEMO_DURATION 3000
typedef void (*Demo)(void);

int demoMode = 0;
int counter = 1;

void setup() 
{
  Serial.begin(115200);
  pinMode(blue, OUTPUT);
  pinMode(vbatPin, INPUT);

  pinMode (16, OUTPUT);
  
  digitalWrite (16, LOW); // set GPIO16 low to reset OLED
  delay (50);
  digitalWrite (16, HIGH); // while OLED is running, GPIO16 must go high
  
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}
char string[25];
void drawFontFaceDemo(float Vbat) {
    // Font Demo1
    // create more fonts at http://oleddisplay.squix.ch/
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, "Battery");
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 10, "Monitoring");
    display.setFont(ArialMT_Plain_24);
    itoa(Vbat,string,10);
    sprintf(string,"%7.5f",Vbat);
    display.drawString(0, 26, string);
}

void draw(void) {
  // graphic commands to redraw the complete screen should be placed here 
  u8g.setFont(u8g_font_unifont);
  //u8g.setFont(u8g_font_osb21);
  u8g.drawFrame(99,0,27,10);
  u8g.drawBox(101,2,23,6);
  u8g.drawBox(126,3,2,4);

}


void percentage(void) {

  u8g.getMode() == U8G_MODE_BW;
    u8g.setColorIndex(1);         // pixel on
  }

void loop() 
{
  digitalWrite(blue, 1);
  delay(100);
  digitalWrite(blue, 0);
  delay(100);
  digitalWrite(blue, 1);
  delay(100);
  
  // Battery Voltage
  VBAT = (float)(analogRead(vbatPin)) / 4095*2*3.3*1.1;
  /*
  The ADC value is a 12-bit number, so the maximum value is 4095 (counting from 0).
  To convert the ADC integer value to a real voltage you’ll need to divide it by the maximum value of 4095,
  then double it (note above that Adafruit halves the voltage), then multiply that by the reference voltage of the ESP32 which 
  is 3.3V and then vinally, multiply that again by the ADC Reference Voltage of 1100mV.
  */
  Serial.println("Vbat = "); Serial.print(VBAT); Serial.println(" Volts");
  display.clear();
  drawFontFaceDemo(VBAT);
   display.display();
  digitalWrite(blue, 0);
  delay(700);

  // picture loop
  u8g.firstPage();

}

```
---

- After editig the I select my board and port for uploading. The board I used *TTGO Lora32-Oled V1*
- Then I uploaded my code and see the voltage of the battery:

---

![18](img/18.jpg)

---

**Diode Bridge in Tinkercad**

We first made a diode Bridge. A diode bridge is an arrangement of four (or more) diodes in a bridge circuit configuration that provides the same polarity of output for either polarity of input.
When used in its most common application, for conversion of an alternating-current (AC) input into a direct-current (DC) output, it is known as a bridge rectifier

For making a diode Bridge. We need to do it step by step:

- Take 4 diodes for example the 1N4007 rectiifer diodes.
- Pick two diodes, make an "L" of their two ends marked with the white bands (cathodes)
- Just as above do it for the remaining two diodes, this time with their ends having no bands (anodes)

- We used tinkercad to make our Diode bridge to see if it work. We also used a 5V regulator.

---

![68](img/68.jpg)

---


- After testing it in Tinkercad we make a phyical diode bridge using a breadboard, diodes, some jumpers and a 5V regulator.

---

![67](img/67.jpg)

---

- We tested our diode bridge with the dc motor

---

![66](img/66.jpg)

---

- After tested the diode bridge. We connected the esp32 to 2 other batteries and connected the diode bridge to the batteries and the jack plug. We also put the usb port to charge the devices

---

![69](img/69.jpg)

--- 


## 2.5 Files & Code
 
- Here in this file are all the codes.

<a href="myFile.js" download>Codes</a>

## 2.7 References & Credits

Above we have all the refrences
